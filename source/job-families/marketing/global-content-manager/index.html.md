---
layout: job_family_page
title: "Manager, Global Content"
---

As the lead of the global content team you will lead the team responsible for content strategy, development, and governance. 
This role will drive audience, prospect, and customer interaction through thoughtful multimedia storytelling and work cross-functionally to identify content needs, strategy, and process. The qualified
candidate will be a driven and engaged senior manager with a successful record of leading and building content programs and teams globally.
Reporting to the Senior Director, Corporate Marketing, the Global Content Manager must have experience across editorial, content marketing, and video production.

## Manager, Global Content

### Responsibilities 

- Define GitLab's global content strategy 
- Act as a steward of the GitLab brand ensuring quality and cohesiveness across all content development 
- Oversee global content creation, governance, and operations 
- Leading and launching content marketing programs
- Effectively and efficienctly partner with brand & digital, events, and product marketing to deliver on content needs across the customer lifecycle
- Manage & mentor the content marketing, digital production, and editorial teams
- Genereate and implement process improvements to the content pillar model
- Draft quarterly OKRs and content KPIs
- Report on content performance and audience growth
- Use data to measure results and inform decision making and strategy development 

### Requirements 

- 5+ years experience in content marketing & creation, preferrably in the software industry
- Excellent writer and editor 
- Excellent verbal communication skills 
- Experience building a content pillar framework
- Proven track record of developing and executing a successful content strategy 
- Proven ability to work independently with minimal supervision 
- You share our [values](/handbook/values), and work in accordance with those values

## Senior Manager, Global Content

The Senior Manager, Global Content role extends the Manager, Global Content role. 

### Responsibilties

- All responsibilties listed under Manager, Global Content
- Develop a localization strategy for marketing content 
- Define agile content process definition and application 
- Content strategy & inbound content marketing program for about.gitlab.com

### Requirements

- Credibility in craft: Past experience as a content marketer and leading content teams 
- Excellent at cross-functional collaboration 
- Dual minded: ability to focus on process and creative 
- Expert experience as a hiring manager
- A strong people manager 
- Experience managing managers
- Ability to successfully manage at a remote-only company
- Humble, servant leader

### Nice-to-have requirements

- Be a user of GitLab and familiarity with our company
- Experience creating content within the area of application development
- Technical background or solid understanding of developer products; familiarity with Git, Continuous Integraion, Containers, and Kubernetes a plus
- Experience working with global teams
- Prior high-growth start up experience 

## Hiring process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with one of our Global Recruiters.
* A 45 minute interview with our Director, Corporate Marketing 
* A 45 minute interview with our Senior Manager, Corporate Communications 
* A 45 minute interview with our Director, Brand & Digital
* A 45 minute interview with our CMO 
* Finally, our CEO may choose to conduct a final interview
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).