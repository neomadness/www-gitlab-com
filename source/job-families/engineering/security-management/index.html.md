---
layout: job_family_page
title: "Security Engineering Management"
---

## Security Management Roles at GitLab

Managers in the security engineering department at GitLab see the team as their product. While they are technically credible and know the details of what security engineers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of security commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Security Manager

* Hire a world class team of security engineers to work on their team
* Help security engineers grow their skills and experience
* Provide input on security architecture, issues, and features
* Hold regular 1:1's with all members their team
* Create a sense of psychological safety on your team
* Recommend security-related technical and process improvements
* Author project plans for security initiatives
* Draft quarterly OKRs
* Train engineers to screen candidates and conduct managerial interviews
* Strong sense of ownership, urgency, and drive
* Excellent written and verbal communication skills, especially experience with executive-level communications
* Ability to make concrete progress in the face of ambiguity and imperfect knowledge
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

#### Specific security-related efforts or projects

- Secure Development lifecycle (SDL) process guidance
- Style guides and design best practices for engineering
- Courses for engineering (including guest speakers)
- Reduce surface area in application (Git Annex, old API)
- Post-postmortems
- Document security trade-offs
- Automated testing/linting
- Compliance
- Red team activity
- Detection and response
- Network security
- Patch management / Vulnerability management and coordination
- Defense in depth recommendations
- Source code analysis by externals
- Bug bounty program
- Endpoint security
- Runbooks
- Abuse
- Communication
- Credential management
- Identity and access management

### Director of Security

The Director of Security role extends the [Security Manager](#security-engineering-manager) role.

* Own a section of the GitLab Security Department, including security customer assurance, product security or internal security and protection. 
* Run multiple teams within the department: Security Automation, Application Security, Security Operations, Abuse Operations, Compliance, Threat Intelligence, Strategic Security, Security Research, etc.
* Hire a world class team of managers and security engineers to work on their teams
* Secure the company
* Secure our self-managed (on-prem) project and products: GitLab CE/EE
* Secure our user-facing SaaS: GitLab.com
* Manage the security incident response process
* Assess and mitigate constantly changing threats
* Help managers and security engineers grow their skills and experience
* Manage multiple teams and projects
* Hold regular skip-level 1:1's with all members of their team
* Create a sense of psychological safety on your teams
* Drive technical and process improvements
* Drive quarterly OKRs
* Represent the company publicly at conferences

#### Responsibilities

- Secure our products (GitLab CE/EE), services (GitLab.com, package servers, other infrastructure), and company (laptops, email).
- Keep our risk analysis up to date.
- Define and plan priorities for security related activities based on that risk analysis.
- Determine appropriate combination of internal security efforts and external
security efforts including bug bounty programs, external security audits
(penetration testing, black box, white box testing).
- Analyze and advise on new security technologies.
- Build and manage a team, which currently consists of [Security Managers](/job-families/engineering/security-management)
and [Security Engineers](/job-families/engineering/security-engineer).
   - Identify and fill positions.
   - Grow skills in team leads and individual contributors, for example by
   creating training and testing materials.
   - Deliver input on promotions, function changes, demotions, and terminations.
- Ensure our engineers and contributors from the wider community run a secure software development lifecycle for GitLab by training them in best practices and creating automated tools.
- Respond to security and service abuse incidents.
- Perform red team security testing of our product and infrastructure.
- Run our bounty program effectively.
- Ensure we're compliant with our legal and contractual security obligations.
- Evangelise GitLab Security and Values to staff, customers and prospects.

#### Requirements

- Significant application and SaaS security experience in production-level settings.
- This position does not require extensive development experience but the
candidate should be very familiar with common security libraries, security
controls, and common security flaws that apply to Ruby on Rails applications.
- Experience managing teams of engineers, and leading managers.
- Experience with (managing) incident response.
- You share our [values](/handbook/values), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)
- Ability to use GitLab

### VP of Security

#### Responsibilities

* Set the vision of the Gitlab Security Department with a clear roadmap
* Build and maintain a rapidly growing team with top-tier talent
* Run the most transparent security organization in the world
* Establish and implement security policies, procedures, standards, and guidelines
* External communications: Blog, conference speaking, stream company events to YouTube
* Work directly with customers and prospects to address security concerns
* Manage a best-in-class bug bounty program with the highest rewards
* Maintain Investor relations with regard to security
* Act as central point-of-contact to Facility Security Officer for cleared facilities
* Collaborate closely with People Ops, Legal, and any third-party firms to ensure the health and safety of organization’s employees globally
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)

#### Sample Initiatives

* Set up a “Red team” initiative
* Architect and build zero-trust network (ZTN) model
* Best in-class anti-phishing measures
* Test breach remediation
* Ensure regular, automated credential rotation
* Implement a defense-in-depth model
* Implement multi-factor authentication
* Secure and manage internal and external endpoints

#### Must-haves Skills & Experience

GitLab’s senior director of Security must have all of the following attributes.

* At least 10 years prior experience managing information security teams
* Excellent written and verbal communication skills
* Be able to quickly hire top-quality individuals contributors and managers
* Experience managing a multi-level security organization with managers and IC’s
* Collaborate with other groups outside engineering such as Sales, Legal, People Ops, and Finance
* Ability to excel in a remote-only, multicultural, distributed environment
* Possess domain knowledge of common information security management frameworks and regulatory requirements and applicable standards such as ISO 27001, SOC 2, HIPAA, GDPR, PCI, Sarbox, etc.
* Excellent project and program management skills and techniques

#### Nice-to-have Skills & Experience

Great candidates will have some meaningful proportion of the following.

* Working knowledge of the GitLab application
* Relevant Bachelor's degree
* Prior fast-growing startup experience
* US Government security clearance
* Product/Platform company experience
* Self-managed (on-prem) software experience
* SaaS software experience
* Experience with consumer-scale services
* Developer platform/tool industry experience
* Deep open source software (OSS) experience

## Performance Indicators

Security Management has the following job-family performance indicators.

* [Hiring actual vs plan](/handbook/engineering/security/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/security/performance-indicators/#diversity)
* [Handbook update frequency](/handbook/engineering/security/performance-indicators/#handbook-update-frequency)
* [Team member retention](/handbook/engineering/security/performance-indicators/#team-member-retention)
* [HackerOne spend actual vs planned](/handbook/engineering/security/performance-indicators/#hackerone-budget-vs-actual-with-forecast-of-unknowns)
